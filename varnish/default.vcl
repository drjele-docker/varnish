# source https://github.com/mattiasgeniar/varnish-6.0-configuration-templates/blob/master/default.vcl
# source official https://github.com/varnishcache/varnish-cache/blob/6.4/bin/varnishd/builtin.vcl

vcl 4.1;

import std;
import directors;
import xkey;

backend server_one {
    .host = "nginx"; # docker service name for the backend
    .port = "80";
    .max_connections = 300;

    .probe = {
        .request =
            "HEAD / HTTP/1.1"
            "Host: varnish"
            "Connection: close"
            "User-Agent: Varnish Health Probe";

        .interval = 5s;
        .timeout = 1s;
        .window = 5;
        .threshold = 3;
    }

    .first_byte_timeout = 300s;
    .connect_timeout = 5s;
    .between_bytes_timeout = 2s;
}

acl purge {
    "localhost";
    "127.0.0.1";
    "::1";
    "nginx_varnish_ssl"; # docker service name or ip for nginx proxy
}

sub vcl_init {
    new vdir = directors.round_robin();

    vdir.add_backend(server_one);
}

sub vcl_recv {
    set req.backend_hint = vdir.backend();

    if (req.http.X-Forwarded-Proto == "https" ) {
        set req.http.X-Forwarded-Port = "443";
    } else {
        set req.http.X-Forwarded-Port = "80";
    }

    if (req.method == "PRI") {
        return (synth(405));
    }

    if (!req.http.host &&
        req.esi_level == 0 &&
        req.proto ~ "^(?i)HTTP/1.1") {
        return (synth(400));
    }

    if (req.method == "PURGE") {
        if (!client.ip ~ purge) {
            return (synth(405, "This IP(" + client.ip + ") is not allowed to send PURGE requests."));
        }

        return (purge);
    }

    if (req.method != "GET" &&
        req.method != "HEAD" &&
        req.method != "PUT" &&
        req.method != "POST" &&
        req.method != "TRACE" &&
        req.method != "OPTIONS" &&
        req.method != "DELETE" &&
        req.method != "PATCH") {
        return (pipe);
    }

    if (req.method != "GET" && req.method != "HEAD") {
        return (pass);
    }

    if (req.http.Authorization || req.http.Cookie) {
        return (pass);
    }

    return (hash);
}

sub vcl_pipe {
    return (pipe);
}

sub vcl_pass {
    return (fetch);
}

sub vcl_hash {
    hash_data(req.url);

    if (req.http.host) {
        hash_data(req.http.host);
    } else {
        hash_data(server.ip);
    }

    # hash cookies for requests that have them
    if (req.http.Cookie) {
        hash_data(req.http.Cookie);
    }

    return (lookup);
}

sub vcl_purge {
    if (req.http.Xkey-Purge) {
        if (xkey.purge(req.http.Xkey-Purge)) {
            return(synth(200, "Purged: " + req.http.Xkey-Purge));
        } else {
            return(synth(404, "Key not found: " + req.http.Xkey-Purge));
        }
    }

    return (synth(200, "Purged"));
}

sub vcl_hit {
    return (deliver);
}

sub vcl_miss {
    return (fetch);
}

sub vcl_deliver {
    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
    } else {
        set resp.http.X-Cache = "MISS";
    }

    set resp.http.X-Cache-Hits = obj.hits;

    return (deliver);
}

sub vcl_synth {
    set resp.http.Content-Type = "text/html; charset=utf-8";
    set resp.http.Retry-After = "5";
    set resp.body = {"<!DOCTYPE html>
                      <html>
                          <head>
                              <title>"} + resp.status + " " + resp.reason + {"</title>
                          </head>
                          <body>
                              <h1>"} + resp.status + " " + resp.reason + {"</h1>
                              <p>"} + resp.reason + {"</p>
                              <h3>Guru Meditation:</h3>
                              <p>XID: "} + req.xid + {"</p>
                              <hr>
                              <p>Varnish cache server</p>
                          </body>
                      </html>"};

    return (deliver);
}

sub vcl_backend_fetch {
    if (bereq.method == "GET") {
        unset bereq.body;
    }

    return (fetch);
}

sub vcl_backend_response {
    if (bereq.uncacheable) {
        return (deliver);
    } else if (beresp.ttl <= 0s ||
        beresp.http.Set-Cookie ||
        beresp.http.Surrogate-control ~ "(?i)no-store" ||
        (!beresp.http.Surrogate-Control &&
        beresp.http.Cache-Control ~ "(?i:no-cache|no-store|private)") ||
        beresp.http.Vary == "*") {
        set beresp.ttl = 120s;
        set beresp.uncacheable = true;
    }

    return (deliver);
}

sub vcl_backend_error {
    set beresp.http.Content-Type = "text/html; charset=utf-8";
    set beresp.http.Retry-After = "5";
    set beresp.body = {"<!DOCTYPE html>
                        <html>
                            <head>
                                <title>"} + beresp.status + " " + beresp.reason + {"</title>
                            </head>
                            <body>
                                <h1>"} + beresp.status + " " + beresp.reason + {"</h1>
                                <p>"} + beresp.reason + {"</p>
                                <h3>Guru Meditation:</h3>
                                <p>XID: "} + bereq.xid + {"</p>
                                <hr>
                                <p>Varnish cache server</p>
                            </body>
                        </html>"};

    return (deliver);
}

sub vcl_fini {
    return (ok);
}
