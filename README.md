# Varnish Docker Example

This is an example as to how to setup **Varnish** with a **Nginx** backend and a **Nginx reverse proxy**, to allow for **HTTPS**.

**You may clone and modify it as you wish**.

Any suggestions are welcomed.

I want to be able to use the official images and also have the plugins.
